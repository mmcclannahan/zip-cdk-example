#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from '@aws-cdk/core';
import { ZipPocStack } from '../lib/zip-poc-stack';

const app = new cdk.App();
new ZipPocStack(app, 'ZipPocStack');
