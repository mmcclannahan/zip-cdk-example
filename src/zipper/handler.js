const AWS = require('aws-sdk')

exports.handler = function (event, context, callback) {
    var s3 = new AWS.S3();
    var objectParams = {
        Bucket: process.env.BUCKET_NAME,
        Key: 'test.zip'
    }
    s3.getObject(objectParams, function(err, data) {
        if (err){
            console.log(err, err.stack);
        }
        else {
            var response = {
                headers: { 
                    'Content-Type': 'application/zip',
                    'Access-Control-Allow-Origin': "*",
                    'Access-Control-Allow-Methods': 'GET'
                },
                statusCode: 200,
                body: Buffer.from(data.Body).toString('base64'),
                isBase64Encoded: true
            }
            callback(null, response);
        }
    });
}