import { LambdaRestApi, RestApi } from '@aws-cdk/aws-apigateway';
import { Runtime } from '@aws-cdk/aws-lambda';
import { NodejsFunction } from '@aws-cdk/aws-lambda-nodejs';
import { Bucket } from '@aws-cdk/aws-s3';
import * as cdk from '@aws-cdk/core';
import { CfnOutput } from '@aws-cdk/core';

export class ZipPocStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const bucket = new Bucket(this, 'test-bucket', {
      bucketName: 'my-cool-zip-bucket-123'
    });

    const lambda = new NodejsFunction(this, 'zip-processor', {
      entry: './src/zipper/handler.js',
      handler: 'handler',
      environment: {
        BUCKET_NAME: bucket.bucketName
      }
    });

    bucket.grantRead(lambda);

    const api = new LambdaRestApi(this, 'zip-api', {
      handler: lambda,
      binaryMediaTypes: ['*/*']
    });

    new CfnOutput(this, 'gatway-url', {
      value: api.url
    })
  }
}
